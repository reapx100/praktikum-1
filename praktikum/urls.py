"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import re_path
from django.views.generic.base import RedirectView
# from lab_1.views import index as index_lab1
# from lab_2.views import index as index_lab2
import lab_3.urls as lab_3
import lab_4.urls as lab_4
import lab_5.urls as lab_5

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls', namespace='lab-1')),
    re_path(r'^lab-2/', include('lab_2.urls', namespace='lab-2')),
    re_path(r'^lab-2-addon/', include('lab_2_addon.urls', namespace='lab-2-addon')),
    # re_path(r'^$', index_lab1, name='index'),
    # re_path(r'^$', index_lab2, name='index'),
    url(r'^lab-3/', include(lab_3, namespace='lab-3')),
    url(r'^lab-4/', include(lab_4, namespace='lab-4')),
    url(r'^lab-5/', include(lab_5, namespace='lab-5')),
    # url(r'^lab-3/', include(lab_3)),
    re_path(r'^$', RedirectView.as_view(url='lab-2/', permanent=True)),
]