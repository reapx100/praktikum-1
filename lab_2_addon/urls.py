from django.conf.urls import url 
from .views import index 

urlpatterns = [
    url(r'^$', index, name='index'), 
]

app_name = 'lab-2-addon'